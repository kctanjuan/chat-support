<!-- resources/views/chat.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Live Support Chat
        </div>
        <div class="card-body" style="overflow: scroll;height:300px">
            <chat-messages :messages="messages"></chat-messages>
        </div>
        <div class="card-footer">
            <!--  :user="{{ Auth::user() }}" -->
            <chat-form v-on:chatting="addMessage"></chat-form>
        </div>
    </div>
</div>
@endsection