<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Models\Message;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ChatsController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = Message::where('id', '!=', 0)->delete();
        return view('chat');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
        return Message::get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $index = count($request->messages);

        $data = [
            'user_id'   => '1',
            'message'   => $request->messages[$index-1]['message'],
            'chat_name' => $request->chat_name
        ];

        $message = Message::create($data);

        broadcast(new MessageSent($request->chat_name, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }
}
