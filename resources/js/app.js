/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { default: Echo } = require('laravel-echo');

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        frm_message: {
            chat_name: '',
            messages: [],

        },
        messages: [],
    },
    created() {
        this.fetchMessages()
    },
    mounted() {
        this.listenEvent()
    },

    methods: {
        listenEvent() {
            window.Echo.channel('public-chat')
                .listen('.mwell.chatting', (e) => {
                    // console.log(e)
                    this.messages.push({
                        message: e.message.message,
                        chat_name: e.message.chat_name
                    })
                });
        },
        fetchMessages() {
            axios.get('/messages').then(response => {
                this.messages = response.data;
            });
        },

        addMessage(message) {
            this.frm_message.chat_name = localStorage.getItem('chat_name');
            this.frm_message.messages.push(message);
            axios.post('/messages', this.frm_message).then(response => {
                this.fetchMessages()
            });
        }
    },
});