@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Chat Support') }}</div>

                <div class="card-body">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input class="form-control" id="name" type="text" placeholder="Enter your name">
                    </div>
                    <button class="btn btn-outline-primary" id="submit_name">Submit name</button>
                   
                    <!-- <a href="{{ url('/chat') }}">Chat</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        localStorage.removeItem('chat_name')
    })

    $(document).on('click', '#submit_name', function() {

        var name = $('#name').val()
        localStorage.setItem("chat_name", name)
        location.href = "/chat"
    })
</script>
@endsection